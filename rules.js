let pipe = false;
let necklace = false;
let robes = false;
let note = false;
let necklaceMsg = 'The Voice calls to you again, crawling out from deep behind your brain, "Claim the relic... Claim your right..."'
let hidingMsg = "Suddenly you feel a surge of energy from the necklace and it begins to hum quietly. Suddenly, a small door opens behind you."

class Start extends Scene {
    create() {
        this.engine.setTitle(this.engine.storyData.Title); // TODO: replace this text using this.engine.storyData to find the story title
        this.engine.addChoice("Begin your story");
    }

    handleChoice() {
        this.engine.gotoScene(Location, this.engine.storyData.InitialLocation); // TODO: replace this text by the initial location of the story
    }
}

class Location extends Scene {
    create(key) {
        let locationData = this.engine.storyData.Locations[key]; // TODO: use `key` to get the data object for the current story location

        this.engine.show(locationData.Body); // TODO: replace this text by the Body of the location data
        
        if(locationData.Choices && locationData.Choices.length > 0) { // TODO: check if the location has any Choices
            for(let choice of locationData.Choices) { // TODO: loop over the location's Choices
                if (choice.KeyNeeded && choice.KeyNeeded === "Pipe" && !pipe){
                    continue;
                }
                if (choice.KeyNotNeeded && choice.KeyNotNeeded === "Robes" && robes){
                    continue;
                }
                if (choice.KeyNotNeeded && choice.KeyNotNeeded === "Necklace" && necklace){
                    continue;
                }
                if (choice.KeyNotNeeded && choice.KeyNotNeeded === "Note" && note){
                    continue;
                }
                if (choice.KeyNeeded && choice.KeyNeeded === "Necklace" && !necklace){
                    continue;
                }
                if (choice.KeyNeeded && choice.KeyNeeded === "Robes" && !robes){
                    continue;
                }
                this.engine.addChoice(choice.Text, choice.Target); // TODO: use the Text of the choice
                // TODO: add a useful second argument to addChoice so that the current code of handleChoice below works

            }
        } else {
            this.engine.addChoice("The end.")
        }
    }

    handleChoice(choice) {
        if(choice) {
            this.engine.show("&gt; "+choice);
            if (choice === "Sacred Relic Room" && necklace == true){
                this.engine.show(necklaceMsg);
            }
            if (choice === "Hiding" && necklace == true){
                this.engine.show(hidingMsg);
            }
            if (choice === "Pipe"){
                pipe = true;
            }
            if (choice === "Necklace Voice"){
                necklace = true;
            }
            if (choice === "Took Off Necklace"){
                necklace = false;
            }
            if (choice === "Put On Robes"){
                robes = true;
            }
            if (choice === "Read Note"){
                note = true;
            }
            this.engine.gotoScene(Location, choice);
        } else {
            this.engine.gotoScene(End);
        }
    }
}

class End extends Scene {
    create() {
        this.engine.show("<hr>");
        this.engine.show(this.engine.storyData.Credits);
    }
}

Engine.load(Start, 'myStory.json');